# bookstack

[Bookstack](https://www.bookstackapp.com/) Simple & Free Wiki Software

## Access

It is available at [https://bookstack.{{ domain }}/](https://bookstack.{{ domain }}/) or [http://bookstack.{{ domain }}/](http://bookstack.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://bookstack.{{ tor_domain }}/](http://bookstack.{{ tor_domain }}/)
{% endif %}
