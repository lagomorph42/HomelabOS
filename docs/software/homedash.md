# Homedash

[Homedash](https://lamarios.github.io/Homedash2/) is a simple dashboard that allows to monitor and interact with many different services.

## Access

It is available at [https://homedash.{{ domain }}/](https://homedash.{{ domain }}/) or [http://homedash.{{ domain }}/](http://homedash.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://homedash.{{ tor_domain }}/](http://homedash.{{ tor_domain }}/)
{% endif %}
